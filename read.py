import pandas
import numpy as np
import sys


def prent(*x):
  with open("log.txt","a") as f:
    for e in x:
      f.write(str(e))
      f.write("  ")
    print(x)
    f.write("\n")




what="val"
if len(sys.argv)>1:
  what=sys.argv[1]


prent("doing",what)


input_filename = what+".h5"
store = pandas.HDFStore(input_filename)

prent("selecting")
events=store.select("table")#,stop=1000)


prent("converting to matrix")
n=events.as_matrix()
prent("loaded matrix",n.shape)

allx=[]
ally=[]

id=0
for ac in n:
  acx=[]
  for i in range(200):
    sup=[]
    for j in range(4):
      sup.append(ac[4*i+j])
    acx.append(sup)
  allx.append(acx)
  ally.append(ac[-1])
  id+=1
  if id%1000==0:
    prent("did",id)


prent("numping")
allx=np.array(allx)
ally=np.array(ally)
prent("numped")

#prent(allx.shape)
#prent(allx[:,:40,:].shape)
#prent(ally.shape)


prent("saving")

np.savez_compressed(what,x=allx[:,:40,:],xb=allx,y=ally)

prent("saved")
