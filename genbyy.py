import numpy as np
import sys

file="train"
if len(sys.argv)>1:
  file=sys.argv[1]

which=0
if len(sys.argv)>2:
  which=int(sys.argv[2])
  
epsilon=0.001
printmod=1000 
  
f=np.load(file+".npz")
x,xb,y=f["x"],f["xb"],f["y"]


ax,axb,ay=[],[],[]

for i in range(len(y)):
  if abs(y[i]-which)<epsilon:
    ax.append(x[i])
    axb.append(xb[i])
    ay.append(y[i])
  if i%printmod==0:
    print("did",i)    
    

print("saving...")
np.savez_compressed(file+"_au"+str(which),x=ax,xb=axb,y=ay)
print("saved")
