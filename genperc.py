import numpy as np
import sys




perc=[0.01,0.02,0.05,0.1,0.2,0.5,0.8,0.9,0.95,0.98,0.99]
nams=["1","2","5","10","20","50","80","90","95","98","99"]


def tripleshuffle(a,b,c):
  s=np.random.randint(10000,100000)
  np.random.seed(s)
  np.random.shuffle(a)
  np.random.seed(s)
  np.random.shuffle(b)
  np.random.seed(s)
  np.random.shuffle(c)
  return a,b,c
 

q1="val_fair"
q2=""

if len(sys.argv)>1:
  q1=sys.argv[1]
if len(sys.argv)>2:
  q2=sys.argv[2]


q=q1
if len(q2)>0:
  q=q+"_"+q2

print("working",q)

print("loading data")
f0=np.load(q+"_au0.npz")
f1=np.load(q+"_au1.npz")
x0,xb0=f0["x"],f0["xb"]
x1,xb1=f1["x"],f1["xb"]

if "y" in f0.files:
  y0=f0["y"]
else:
  y0=np.zeros((x0.shape[0]))
if "y" in f1.files:
  y1=f1["y"]
else:
  y1=np.ones((x1.shape[0]))


print("loaded data")
print("shuffling")
x0,xb0,y0=tripleshuffle(x0,xb0,y0)
x1,xb1,y1=tripleshuffle(x1,xb1,y1)
print("shuffled")






l0=len(x0)
l1=len(x1)
l=np.min([l0,l1])

for p,nam in zip(perc,nams):
  print("working on",nam)
  n1=int(l*p)
  n0=l-n1

   
  ax=np.concatenate((x0[:n0],x1[:n1]),axis=0)
  axb=np.concatenate((xb0[:n0],xb1[:n1]),axis=0)
  ay=np.concatenate((y0[:n0],y1[:n1]),axis=0)
  print("concattenated")
  ax,axb,ay=tripleshuffle(ax,axb,ay)
  print("shuffled")
  print("saving")
  np.savez_compressed(q+"_p"+str(nam),x=ax,xb=axb,y=ay,p=p,nam=nam)
  print("saved")
  






















