#!/usr/bin/env zsh
#SBATCH --mem-per-cpu=40G
#SBATCH --job-name=Read_job
#SBATCH --output=output.%J.txt
#SBATCH --gres=gpu:1
#SBATCH --time 240





module load cuda/100
module load cudnn/7.4
module load python/3.6.8

cd /home/sk656163/m/toptagdataref/

rm log.txt
python3 read.py train
python3 read.py test
